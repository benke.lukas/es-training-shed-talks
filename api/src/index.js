const express = require('express')
const cors = require('cors')
const {Client} = require('@elastic/elasticsearch')
const formatterService = require('./formatterService')

const app = express()
const PORT = 3000

const client = new Client({node: 'http://es:9200'})
const INDEX_NAME = 'shed-talks'

////////////////////////////////////
// MODIFY HERE
////////////////////////////////////
/**
 * @param searchQuery String|null - searched query e.g. "fossil fuels"
 * @param tags Array|null - list of tags e.g. ['marketing', 'leadership'] - treated as AND
 * @param ratings Array|null - list of ratings e.g. [['Inspiring', 100], ['Informative', 150]] - treated as OR
 */
async function search(
  searchQuery,
  tags,
  ratings,
) {
  let query = {
    bool: {
      must: [],
      filter: [],
      should: []
    }
  };

  if (searchQuery !== null) {
    query.bool.must.push({
      multi_match: {
        query: searchQuery,
        fields: [
          "description",
          "main_speaker",
          "name",
          "title",
          "url",
          "transcript"
        ]
      }
    });
  }

  if (tags !== null) {
    query.bool.filter.push({
      terms_set: {
        tags: {
          terms: tags,
          minimum_should_match_script: {
            source: "params.num_terms" // number of matches on tags field should be equal to number of passed tags => effective AND between tags
          }
        }
      }
    })
  }

  if (ratings !== null) {
    ratings.forEach(
      (rating) => {
        [label, count] = rating;

        query.bool.filter.push({
          bool: {
            should: [
              {
                bool: {
                  must: [
                    {
                      term: {
                        "ratings.name": {
                          "value": label
                        }
                      }
                    },
                    {
                      range: {
                        "ratings.count": {
                          "gt": count
                        }
                      }
                    }
                  ]
                }
              }
            ]
          }
        })
      }
    )
  }

  //console.log('ConstructedQuery: ', JSON.stringify(query, null, 2));

  let results = client.search({
    index: INDEX_NAME,
    body: {
      query: query
    }
  });

  return {
    results: results,
    query: query
  }
}

////////////////////////////////////

app.use(cors({origin: '*'}))

app.get('/', (req, res) => {
  let tags = null;
  let ratings = null;

  if (req.query.tags) {
    tags = req.query.tags.split(',')
  }

  if (req.query.ratings) {
    ratings = req.query.ratings.split(',').map((item) => [item.split(':')[0], Number.parseInt(item.split(':')[1])])
  }

  console.log('q: ', req.query.q || null, 'tags: ', tags, 'ratings: ', ratings)

  search(
    req.query.q || null,
    tags,
    ratings,
  ).then((search) => {
    search.results.then((data) => {
      res.send({
        results: formatterService.formatResults(data),
        query: search.query
      })
    })
  }).catch((err) => {
    res.send(err)
  })
})

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})
