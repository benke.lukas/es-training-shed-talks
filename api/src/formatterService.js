function formatResults (results) {
  return results.body.hits.hits.map((item) => {
    return {
      title: item._source.title,
      comments: item._source.comments,
      duration: item._source.duration,
      views: item._source.views,
      tags: item._source.tags,
      ratings: item._source.ratings
    }
  })
}

module.exports = { formatResults }
