# Shed talks

## Setup

Setup should be quite easy, just run `docker-compose up -d` in the root dir and give it a while to start up. 
Once you verify it's working you can stop it `docker-compose stop` or stop and remove it `docker-compose down`.

## Is it working?

- check `localhost:3000` and you should get a response like `{"name":"ConnectionError", ...`
  - `ConnectionError` is fine :)
- check `localhost:3001` and search for something (press `enter` to search)
  - you should someting like this ![](assets/ui-example.png)
- check `localhost:3002` and you should see a response like

  `{"name" : "4d3898210f8d",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "4Jcaj0LnTO2Q781socArng",`
- check `localhost:3003` and you should see Kibana UI


## Isn't it working?

![](assets/too-bad.png)

Drop me a message :)

## Homework

Homework is optional, but you can practise your newly gained skill :)

### Goal
- create an index with mapping of your choice
  - index has to be named `shed-talks`
  - has to be created manually via dev-tools or `curl`
- index documents
  - run in `indexer` folder `docker-compose exec indexer npm run index-large-dataset`
  - this should index documents provided in this repository
- create a query for ES in order to find relevant documents `api/src/index.js`
  - query structure is totally up to you (experiment :))
  - ![](assets/form.png)
    - `Search` - user inputted query e.g. "fossil fuels" (like Google search)
    - `Tags` - comma separated list of tags, document must have all e.g. `marketing,leadership`
    - `Ratings` - comma separated list of ratings in format `rating-name:minimum-value`, document 
      must match at least one of provided ratings all e.g. `Inspiring:100,Informative:150`
      Document must have `Inspiring` with at least 100 OR `Informative` with at least 150 in
      order to be considered as a match.
- bored? Create more goals for yourself ;)

### Share a progress
I'll be glad if you share your progress. 

### Notes
App is really simple and entering some values may crash it. It's intended for demo purposes.
It's definitely not bulletproof. If you find anything, that blocks you, please let me know, I'll create a fix for that.
