import React from 'react'
import './App.css'
import Header from './Header'
import Item from './Item'
import SearchForm from './Form'

const style = {
  backgroundColor: '#1f4662',
  color: '#fff',
  fontSize: '12px'
}

const headerStyle = {
  backgroundColor: '#193549',
  padding: '5px 10px',
  fontFamily: 'monospace',
  color: '#ffc600'
}

const preStyle = {
  display: 'block',
  padding: '10px 30px',
  margin: '0',
  overflow: 'scroll'
}

class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = { items: [], query: null, error: null }
  }

  render () {
    const searchCallback = query => {
      const url = new URL('http://localhost:3000')
      url.search = new URLSearchParams(query).toString()

      fetch(url).then(response => response.json()).then(
        data => {
          const results = data.results;
          if (!Array.isArray(results)) {
            console.log(results)
            this.setState({ items: [], error: JSON.stringify(results, null, 2) })

            return
          }

          const items = []

          results.forEach(item => items.push(<Item item={item} />))

          this.setState({ ...this.state, query: JSON.stringify(data.query, null, 2), items: items})
        }
      )
    }

    const errorDiv = this.state.error
      ? (
        <div style={style}>
          <div style={headerStyle}>
            <strong>Pretty Debug</strong>
          </div>
          <pre style={preStyle}>
            {this.state.error}
          </pre>
        </div>
        )
      : null

    const queryDiv = this.state.query
      ? (
        <div style={style}>
          <div style={headerStyle}>
            <strong>Query</strong>
          </div>
          <pre style={preStyle}>
            {this.state.query}
          </pre>
        </div>
        )
      : null

    return (
      <div className='App'>
        <Header />
        <SearchForm searchCallback={searchCallback} />
        <div style={{ fontSize: 'large', borderTop: 'solid' }}>Number of results: {this.state.items.length}</div>
        <div style={{ fontSize: 'large', borderBottom: 'solid' }}>{(new Date()).toLocaleTimeString('cz-CS')}</div>
        <div style={{ textAlign: 'left' }}>
          {this.state.items}
          {queryDiv}
          {errorDiv}
        </div>
      </div>
    )
  }
}

export default App
