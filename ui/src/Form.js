import React from 'react'

const initialState = { q: '', tags: '', ratings: '' }

class SearchForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = initialState

    this.searchCallback = props.searchCallback
  }

  handleChangeSearch = (event) => {
    this.setState({ ...this.state, q: event.target.value })
  }

  handleChangeTags = (event) => {
    this.setState({ ...this.state, tags: event.target.value })
  }

  handleChangeRatings = (event) => {
    this.setState({ ...this.state, ratings: event.target.value })
  }

  handleSubmit = (event) => {
    alert('A name was submitted: ' + this.state.value)
    event.preventDefault()
  }

  onKeyDown = (event) => {
    if (event.key === 'Enter') {
      this.searchCallback(this.state)
    }
  }

  search = () => {
    this.searchCallback(this.state)
  }

  handleReset = () => {
    this.setState(initialState, this.search)
  }

  render () {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Search:
          <input type='text' value={this.state.q} onChange={this.handleChangeSearch} onKeyDown={this.onKeyDown} />
        </label>
        <label>
          Tags:
          <input type='text' value={this.state.tags} onChange={this.handleChangeTags} onKeyDown={this.onKeyDown} />
        </label>
        <label>
          Ratings:
          <input type='text' value={this.state.ratings} onChange={this.handleChangeRatings} onKeyDown={this.onKeyDown} />
        </label>
        <button type='button' onClick={this.handleReset}>Clear</button>
      </form>
    )
  }
}

export default SearchForm
