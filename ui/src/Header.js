import logo from './img.png'
import './App.css'

function Header () {
  return (
    <header className='App-header'>
      <img src={logo} className='App-logo' alt='logo' />
      <h2>Shed talks: <span>Benekiiii Edition</span></h2>
    </header>
  )
}

export default Header
