# Shed Talks mapping
```shell
PUT /shed-talks
{
  "settings": {
    "number_of_replicas": 0
  },
  "mappings": {
    "dynamic": false,
    "properties": {
      "comments": {
        "type": "integer"
      },
      "description": {
        "type": "text"
      },
      "duration": {
        "type": "integer"
      },
      "event": {
        "type": "keyword"
      },
      "film_date": {
        "type": "date",
        "format": "epoch_second"
      },
      "languages": {
        "type": "integer"
      },
      "main_speaker": {
        "type": "text"
      },
      "name": {
        "type": "text"
      },
      "num_speaker": {
        "type": "integer"
      },
      "published_date": {
        "type": "date",
        "format": "epoch_second"
      },
      "ratings": {
        "properties": {
          "id": {
            "type": "integer"
          },
          "name": {
            "type": "keyword"
          },
          "count": {
            "type": "integer"
          }
        }
      },
      "speaker_occupation": {
        "type": "keyword"
      },
      "tags": {
        "type": "keyword"
      },
      "title": {
        "type": "text"
      },
      "url": {
        "type": "text"
      },
      "views": {
        "type": "integer"
      },
      "transcript": {
        "type": "text"
      }
    }
  }
}
```