const { Client } = require('@elastic/elasticsearch')
const lineReader = require('line-reader')

const client = new Client({ node: 'http://es:9200' })
const INDEX_NAME = 'shed-talks'

async function clearIndex () {
  return client.deleteByQuery({
    index: INDEX_NAME,
    body: { query: { match_all: {} } }
  })
}

async function indexDoc (document) {
  client.index({
    index: INDEX_NAME,
    body: document
  })
}

const indexPromises = []

clearIndex().then(() => {
  lineReader.eachLine(process.argv[2], (line, last) => {
    indexPromises.push(indexDoc(JSON.parse(line)))

    if (last) {
      Promise.all(indexPromises)
        .then(() => {
          console.log(`${indexPromises.length} documents were indexed.`)
        })
        .catch(e => {
          console.log(e)
        })
    }
  })
})
